package com.jiarupc.model;


import com.jiarupc.excel.annotation.ExcelField;

/**
 * @Author: jiarupc
 * @Date: 2020/11/27
 * @Description:    excel导出-订单模型
 */
public class OrderModel {
    @ExcelField(title = "订单号", align = 2, sort = 20)
    private String orderNo;

    @ExcelField(title = "金额", align = 2, sort = 20)
    private String amount;

    // @ExcelField(title = "创建时间", align = 2, sort = 20)
    private String createTime;

    public OrderModel(String orderNo, String amount, String createTime) {
        this.orderNo = orderNo;
        this.amount = amount;
        this.createTime = createTime;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public String getAmount() {
        return amount;
    }

}
