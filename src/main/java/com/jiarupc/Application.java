package com.jiarupc;

import com.jiarupc.excel.ExcelExporter;
import com.jiarupc.model.OrderModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: jiarupc
 * @Date: 2020/11/29
 * @Description:    演示demo
 */
public class Application {

    public static void main(String[] args) throws IOException {
        // 生成数据
        List<OrderModel> orderList = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            orderList.add(new OrderModel("000" + i, "100.00", "2020-11-29"));
        }

        // 导出excel表
        new ExcelExporter("商品订单表", OrderModel.class)
                .setDataList(orderList)
                .writeFile( "e:/商品订单表.xlsx").dispose();

    }
}
