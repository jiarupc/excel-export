package com.jiarupc.excel.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author: jiarupc
 * @Date: 2020/11/11
 * @Description: Excel注解
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelField {

	/**
	 * 导出字段标题（导出时，如果要添加批注请用“**”分隔，例如，标题**批注）
	 */
	String title();

	/**
	 * 导出字段排序（升序）
	 */
	int sort() default 0;

	/**
	 * 导出字段对齐方式（0：自动；1：靠左；2：居中；3：靠右）
	 */
	int align() default 0;

}
