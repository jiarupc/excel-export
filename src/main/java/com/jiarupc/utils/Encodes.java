package com.jiarupc.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * @Author: jiarupc
 * @Date: 2019/12/23
 * @Description:    编码解码工具类：精简版
 */
public class Encodes {

	private static final String DEFAULT_URL_ENCODING = "UTF-8";


	/**
	 * URL 编码, Encode默认为UTF-8. 
	 */
	public static String urlEncode(String part) {
		try {
			return URLEncoder.encode(part, DEFAULT_URL_ENCODING);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException();
		}
	}
}
